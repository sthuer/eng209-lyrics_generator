#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Simon Thür, Tim Frey"
__copyright__ = "Copyright (c) 2021"
__version__ = "1.0.0"


from dataclasses import dataclass
from enum import Enum
from typing import Generator, Iterable, List, Tuple


# ================================================================
# Partie A
# Malbrough
# ================================================================
def malbrough(spec: List[Tuple[str, str]]) -> Generator[str, None, None]:
    """Generate lines for malbrough.

    Args:
        spec (List[Tuple[str, str]]): Verses of malbrough

    Yields:
        str: Lines of malbrough
    """

    return (
        verse
        for line in spec
        for verse in (
            line[0],
            'Mironton, mironton, mirontaine',
            line[0],
            *(line[1] for _ in range(3)),
            ""
        )
    )


# ================================================================
# Partie B
# Print lyrics
# ================================================================
def repetition(count: int) -> str:
    """Translate repetition count to text

    Args:
        count (int): number of repetitions

    Returns:
        str: Musician readable repetition count
    """
    if count > 4:
        return f" ({count} fois)"
    elif count == 4:
        return " (quater)"
    elif count == 3:
        return " (ter)"
    elif count == 2:
        return " (bis)"
    else:
        return ""


def format_lyrics(lines: Iterable[str]) -> Generator[str, None, None]:
    """Format lyrics and abbreviate repetitive lines.

    Args:
        lines (List[str]): Song text to format, each line as an element.

    Yields:
        str: formatted lyrics as string
    """

    capitalize = lambda s: s[0].upper() + s[1:] if s else ""

    count = 0
    last_line = None
    for line in lines:
        if line == last_line:
            count += 1
        else:
            if count > 0:
                yield capitalize(last_line) + repetition(count)
            count = 1
        last_line = line
    yield capitalize(last_line) + repetition(count)


def print_lyrics(lines: Iterable[str]) -> None:
    """Format and print lyrics

    Args:
        lines (List[str]): lyrics
    """
    print(*format_lyrics(lines), sep="\n")


# ================================================================
# Partie C
# Lorraine
# ================================================================
def lorraine(phrases: List[str]) -> Generator[str, None, None]:
    """Creates a 'lorraine'-like poème based on key phrases
    Generates one paragraph less then key phrases provided

    Parameters:
        phrases (List[str]): List of key phrases

    Yields:
        str: List of verses
    """
    return (
        verse
        for i, phrase in enumerate(phrases[:-1])
        for verse in (
            f"{phrase} avec mes sabots",
            f"{phrase} avec mes sabots",
            phrases[i + 1],
            "Avec mes sabots, dondaine",
            "Oh oh oh, avec mes sabots",
            ""
        )
    )


# ================================================================
# Partie D
# Mariton
# ================================================================
def pied_mariton(attributs: List[str]) -> Generator[str, None, None]:
    """Creates a poème like "pied mariton",
    which adds up attributs paragraph by paragraph

    Parameters:
        attributs (List[str]): attributes

    Yields:
        str: List of verses
    """
    return (
        verse
        for i in range(len(attributs))
        for verse in (
            f"La Marie-Madeleine, elle a {attributs[i]}",   # new attribut
            *reversed(attributs[:i+1]),                     # list old ones
            f"{attributs[0]} Madeleine, {attributs[0]} Madelon",
            f"{attributs[0]} Madeleine, {attributs[0]} Madelon",
            "",
        )
    )


# ================================================================
# Partie E
# General lyrics generator
# ================================================================
@dataclass
class Verse:
    class Type(Enum):
        NORMAL = 0
        REVERSED_LIST = 1
    line: str
    verse_type: Type = Type.NORMAL
    repetition_count: int = 1


def gen_lyrics(desired_format: List[Verse], data: List[List[str]]) -> Generator[str, None, None]:
    """Generate lyrics based on a desired format and a given list of lines

    Args:
        desired_format (List[Verse]): Format in terms of strings or Verse:
            [
            "{0}",
            "Mironton, mironton, mirontaine",
            "{0}",
            Verse("{1}", repetition_count=3),
            ],
            where {n} calls for the element [i][n] where n is the iteration.
            to call for a specific verse {i}{n} can be used.

        data (List[List[str]]): Actual lines

    Yields:
        str: Generated lyrics, line by line
    """
    for i, paragraph in enumerate(data):
        for j in desired_format:
            verse = j if isinstance(j, Verse) else Verse(j)
            if verse.verse_type == Verse.Type.REVERSED_LIST:
                for element in reversed(data[:i+1]):
                    yield verse.line.format(*element)
            else:
                for _ in range(verse.repetition_count):
                    yield verse.line.format(*paragraph, data=data)
        yield ""


# ================================================================
# Partie F
# Find Chorus
# ================================================================
def split_into_paragraphs(lines: Iterable[str]) -> Generator[List[str], None, None]:
    """Split a list of lines into several lists of lines, splitting on empty lines

    Args:
        lines (Iterable[str]): All lines

    Yields:
        [List[str]]: Lists of lines, split after empty lines
    """
    paragraph = []
    for line in lines:
        if line == "":
            yield paragraph
            paragraph = []
        else:
            paragraph.append(line)


def find_refrain(lyrics: Iterable[str]) -> Generator[str, None, None]:
    """Find repeating blocks of lines (refrain) and replace them with "(Refrain)"

    Args:
        lyrics (Iterable[str]): All lyrics of song

    Yields:
        str: Lines of song abbreviated with Refrains
    """
    paragraphs = list(split_into_paragraphs(lyrics))
    refrain = None
    for i, paragraph in enumerate(paragraphs):
        if refrain and paragraph == refrain:
            yield "(Refrain)"
        else:
            if not refrain:
                for other_paragraph in paragraphs[i+1:]:
                    if paragraph == other_paragraph:
                        refrain = paragraph
                        yield "Refrain:"
                        break
            for line in paragraph:
                yield line
        yield ""


def print_lyrics_with_chorus(lines: Iterable[str]) -> None:
    """Format and print lyrics

    Args:
        lines (List[str]): lyrics
    """
    print(*find_refrain(format_lyrics(lines)), sep="\n")


# ================================================================
# Main
# ================================================================
def main():
    # Partie a + b
    print_lyrics(
        malbrough([
            ("Malbrough s'en va-t-en guerre", "Ne sait quand reviendra"),
            ("Il reviendra z'à Pâques", "Ou à la Trinité"),
            ("La Trinité se passe", "Malbrough ne revient pas"),
            ("Madame à sa tour monte", "Si haut qu'elle peut monter")
        ])
    )

    # Partie c
    print_lyrics(
        lorraine([
            "En passant par la Lorraine",
            "Rencontrai trois capitaines",
            "Ils m'ont appelée vilaine",
            "Je ne suis pas si vilaine"
        ])
    )

    # Partie d
    print_lyrics(
        pied_mariton([
            "un pied mariton",
            "une jambe de boué",
            "un genou cagneux",
            "une cuisse de v'lours"
        ])
    )

    # Partie e (same as malbrough)
    print_lyrics(
        gen_lyrics(
            desired_format=[
                "{0}",
                "Mironton, mironton, mirontaine",
                "{0}",
                Verse("{1}", repetition_count=3),
            ],
            data=[
                ("Malbrough s'en va-t-en guerre", "Ne sait quand reviendra"),
                ("Il reviendra z'à Pâques", "Ou à la Trinité"),
                ("La Trinité se passe", "Malbrough ne revient pas"),
                ("Madame à sa tour monte", "Si haut qu'elle peut monter")
            ]
        )
    )

    # Partie e (same as lorraine)
    print_lyrics(
        gen_lyrics(
            desired_format=[
                Verse("{0} avec mes sabots", repetition_count=2),
                "{1}",
                "Avec mes sabots, dondaine",
                "Oh oh oh, avec mes sabots",
            ],
            data=[
                ("En passant par la Lorraine", "Rencontrai trois capitaines"),
                ("Rencontrai trois capitaines", "Ils m'ont appelée vilaine"),
                ("Ils m'ont appelée vilaine", "Je ne suis pas si vilaine"),
            ]
        )
    )

    # Partie e (same as pied_mariton)
    print_lyrics(
        gen_lyrics(
            desired_format=[
                "La Marie-Madeleine, elle a {0}",
                Verse("{0}", Verse.Type.REVERSED_LIST),
                Verse("{data[0][0]} Madeleine, {data[0][0]} Madelon",
                      repetition_count=2),
            ],
            data=[
                ("un pied mariton", ),
                ("une jambe de boué", ),
                ("un genou cagneux", ),
                ("une cuisse de v'lours", )
            ]
        )
    )

    # Partie e
    print_lyrics(
        gen_lyrics(
            desired_format=[
                "{0}",
                "{1} {2}",
                "{1}, oui, oui, oui",
                "{1}, non, non, non",
                "{1} {2}",
            ],
            data=[
                ("Chevaliers de la table ronde",
                 "Goûtons voir", "si le vin est bon"),
                ("S'il est bon, s'il est agréable",
                 "J'en boirai", "jusqu'à mon plaisir"),
                ("J'en boirai cinq à six bouteilles",
                 "Une femme", "sur mes genoux"),
                ("S'il est bon, s'il est agréable",
                 "J'en boirai", "jusqu'à mon plaisir"),
            ]
        )
    )

    # Partie f
    print_lyrics_with_chorus(
        gen_lyrics(
            desired_format=[
                Verse("{0}", repetition_count=2),
                "{1}",
                "",
                "Auprès de ma blonde",
                "Qu'il fait bon, fait bon, fait bon",
                "Auprès de ma blonde",
                "Qu'il fait bon dormir",
            ],
            data=[
                (
                    "Au jardins de mon père, les lilas sont fleuris",
                    "Tous les oiseaux du monde viennent y fair' leurs nids",
                ),
                (
                    "La caille, la tourterelle et la jolie perdrix",
                    "Et la blanche colombe, qui chante jour et nuit"
                )
            ]
        )
    )


if __name__ == "__main__":
    main()
