#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Simon Thür, Tim Frey"
__copyright__ = "Copyright (c) 2021"
__version__ = "0.0.2"


import difflib

from lyrics_gen import *


tests = [
    {
        "name": "malbrough",
        "generated": malbrough([
            ("Malbrough s'en va-t-en guerre", "Ne sait quand reviendra"),
            ("Il reviendra z'à Pâques", "Ou à la Trinité"),
            ("La Trinité se passe", "Malbrough ne revient pas"),
            ("Madame à sa tour monte", "Si haut qu'elle peut monter")
        ]),
        "expected": [
            "Malbrough s'en va-t-en guerre", 'Mironton, mironton, mirontaine',
            "Malbrough s'en va-t-en guerre", 'Ne sait quand reviendra',
            'Ne sait quand reviendra', 'Ne sait quand reviendra',
            '',
            "Il reviendra z'à Pâques", 'Mironton, mironton, mirontaine',
            "Il reviendra z'à Pâques", 'Ou à la Trinité',
            'Ou à la Trinité', 'Ou à la Trinité',
            '',
            'La Trinité se passe', 'Mironton, mironton, mirontaine',
            'La Trinité se passe', 'Malbrough ne revient pas',
            'Malbrough ne revient pas', 'Malbrough ne revient pas',
            '',
            'Madame à sa tour monte', 'Mironton, mironton, mirontaine',
            'Madame à sa tour monte', "Si haut qu'elle peut monter",
            "Si haut qu'elle peut monter", "Si haut qu'elle peut monter",
            ''
        ]
    },
    {
        "name": "malbrough gen",
        "generated": format_lyrics(gen_lyrics(
            desired_format=[
                "{0}",
                "Mironton, mironton, mirontaine",
                "{0}",
                Verse("{1}", repetition_count=3),
            ],
            data=[
                ("Malbrough s'en va-t-en guerre", "Ne sait quand reviendra"),
                ("Il reviendra z'à Pâques", "Ou à la Trinité"),
                ("La Trinité se passe", "Malbrough ne revient pas"),
                ("Madame à sa tour monte", "Si haut qu'elle peut monter")
            ]
        )),
        "expected": list(format_lyrics(malbrough([
            ("Malbrough s'en va-t-en guerre", "Ne sait quand reviendra"),
            ("Il reviendra z'à Pâques", "Ou à la Trinité"),
            ("La Trinité se passe", "Malbrough ne revient pas"),
            ("Madame à sa tour monte", "Si haut qu'elle peut monter")
        ]))),
    },
    {
        "name": "loraine",
        "generated": format_lyrics(lorraine(
            ["En passant par la Lorraine",
             "Rencontrai trois capitaines",
             "Ils m'ont appelée vilaine",
             "Je ne suis pas si vilaine"]
        )),
        "expected": [
            "En passant par la Lorraine avec mes sabots (bis)",
            "Rencontrai trois capitaines",
            "Avec mes sabots, dondaine",
            "Oh oh oh, avec mes sabots",
            "",
            "Rencontrai trois capitaines avec mes sabots (bis)",
            "Ils m'ont appelée vilaine",
            "Avec mes sabots, dondaine",
            "Oh oh oh, avec mes sabots",
            "",
            "Ils m'ont appelée vilaine avec mes sabots (bis)",
            "Je ne suis pas si vilaine",
            "Avec mes sabots, dondaine",
            "Oh oh oh, avec mes sabots",
            ""
        ]
    },
    {
        "name": "loraine gen",
        "generated": format_lyrics(gen_lyrics(
            desired_format=[
                Verse("{0} avec mes sabots", repetition_count=2),
                "{1}",
                "Avec mes sabots, dondaine",
                "Oh oh oh, avec mes sabots",
            ],
            data=[
                ("En passant par la Lorraine", "Rencontrai trois capitaines"),
                ("Rencontrai trois capitaines", "Ils m'ont appelée vilaine"),
                ("Ils m'ont appelée vilaine", "Je ne suis pas si vilaine"),
            ]
        )),
        "expected": list(format_lyrics(lorraine(
            ["En passant par la Lorraine",
             "Rencontrai trois capitaines",
             "Ils m'ont appelée vilaine",
             "Je ne suis pas si vilaine"])
        )),
    },
    {
        "name": "t0",
        "generated": (x for x in range(0)),
        "expected": []
    },
    {
        "name": "pied_mariton",
        "generated": format_lyrics(pied_mariton([
            "un pied mariton",
            "une jambe de boué",
            "un genou cagneux",
            "une cuisse de v'lours"
        ])),
        "expected": [
            'La Marie-Madeleine, elle a un pied mariton',
            'Un pied mariton',
            'Un pied mariton Madeleine, un pied mariton Madelon (bis)',
            '',
            'La Marie-Madeleine, elle a une jambe de boué',
            'Une jambe de boué',
            'Un pied mariton',
            'Un pied mariton Madeleine, un pied mariton Madelon (bis)',
            '',
            'La Marie-Madeleine, elle a un genou cagneux',
            'Un genou cagneux',
            'Une jambe de boué',
            'Un pied mariton',
            'Un pied mariton Madeleine, un pied mariton Madelon (bis)',
            '',
            "La Marie-Madeleine, elle a une cuisse de v'lours",
            "Une cuisse de v'lours",
            'Un genou cagneux',
            'Une jambe de boué',
            'Un pied mariton',
            'Un pied mariton Madeleine, un pied mariton Madelon (bis)',
            ''
        ]
    },
    {
        "name": "pied_mariton gen",
        "generated": format_lyrics(gen_lyrics(
            desired_format=[
                "La Marie-Madeleine, elle a {0}",
                Verse("{0}", Verse.Type.REVERSED_LIST),
                Verse("{data[0][0]} Madeleine, {data[0][0]} Madelon",
                      repetition_count=2),
            ],
            data=[
                ("un pied mariton", ),
                ("une jambe de boué", ),
                ("un genou cagneux", ),
                ("une cuisse de v'lours", )
            ]
        )),
        "expected": list(format_lyrics(pied_mariton([
            "un pied mariton",
            "une jambe de boué",
            "un genou cagneux",
            "une cuisse de v'lours"
        ]))),
    },
    {
        "name": "refrain",
        "generated":  find_refrain(format_lyrics(gen_lyrics(
            desired_format=[
                Verse("{0}", repetition_count=2),
                "{1}",
                "",
                "Auprès de ma blonde",
                "Qu'il fait bon, fait bon, fait bon",
                "Auprès de ma blonde",
                "Qu'il fait bon dormir",
            ],
            data=[
                (
                    "Au jardins de mon père, les lilas sont fleuris",
                    "Tous les oiseaux du monde viennent y fair' leurs nids",
                ),
                (
                    "La caille, la tourterelle et la jolie perdrix",
                    "Et la blanche colombe, qui chante jour et nuit"
                )
            ]
        ))),
        "expected": [
            "Au jardins de mon père, les lilas sont fleuris (bis)",
            "Tous les oiseaux du monde viennent y fair' leurs nids",
            "",
            "Refrain:",
            "Auprès de ma blonde",
            "Qu'il fait bon, fait bon, fait bon",
            "Auprès de ma blonde",
            "Qu'il fait bon dormir",
            "",
            "La caille, la tourterelle et la jolie perdrix (bis)",
            "Et la blanche colombe, qui chante jour et nuit",
            "",
            "(Refrain)",
            ""
        ]
    }
]


def main():
    err_cnt = 0
    
    for test in tests:
        generated = list(test["generated"])
        if generated != test["expected"]:
            err_cnt += 1
            print(f"\nTest for {test['name']} failed:")
            for line in difflib.ndiff(generated, test["expected"]):
                print(line)

    if err_cnt:
        print(f"{err_cnt} test{'s' if err_cnt > 1 else ''} failed :(")
        exit(1)
    print("All tests successful!")


if __name__ == '__main__':
    main()
