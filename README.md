# ENG-209, Partie 1, Projet pour évaluation intermédiaire

_Note préliminaire: ce descriptif contient des extraits de textes de chansons traditionnelles dans la francophonie. En aucun cas cela n'implique un plébiscite des idées, des auteurs ou même de la forme de ces textes, qui ont été choisis uniquement pour les propriétés de leurs structures répétitives._

## Contexte & organisation

L'évaluation du ENG-209 se fait en deux parties: une évaluation intermédiaire, décrite dans ce document, qui compte pour un tiers de la note, et une évaluation en fin de semestre, décrite plus tard, qui compte pour deux tiers.

Cette évaluation intermédiaire prend la forme de la résolution, par groupe de deux, de la tâche décrite à la section suivante. Le résultat sera présenté lors d'une séance Zoom le lundi 25 octobre 2021 entre 10h00 et 14h00. Chaque groupe de deux personnes disposera de 4 minutes pour présenter le travail de façon conjointe, et de 5 minutes supplémentaires pour répondre à des questions de l'enseignant sur le code fourni.

Une activité Moodle est mise en place pour la formation des groupes; elle est à compléter dans la journée du lundi 11 octobre 2021.

Une autre activité Moodle est mise en place pour permettre à chaque groupe de s'inscrire à un créneau horaire. Elle est à compléter avant le lundi 18 octobre 2021 à 13h.

Indépendamment du créneau horaire, un fichier Python (`.py` ou `.ipynb`) contenant le travail accompli devra être déposé via Moodle pas plus tard que le **mercredi 20 octobre à 23h59**.

Lors de la présentation, la présence des deux membres dans la salle Zoom indiquée est requise **5 minutes avant** le début de la présentation. Les membres du groupes sont responsables du bon fonctionnement de leur machine et de leur capacité à pouvoir partager l'écran de leur machine, à faire tourner leur code, et à y faire de petites modifications à la volée pendant l'examen si demandé.

L'évaluation intermédiaire est notée sur une échelle en demi-points de 1 à 6. Le travail à préparer se compose d'un problème de base à résoudre et d'une extension, représentant une partie plus créative. Résoudre la tâche de base apporte un nombre de points $p_b$ d'au plus 2.5, l'extension peut rapporter un nombre de points $p_e$ jusqu'à 3.5. La note finale $n$ est $n = \min(6, 1 + p_b + p_e)$.

En cas d'absence dûment justifiée, cette évaluation sera remplacée par entretien oral à distance dans les semaines qui suivront le 19 octobre.


## Tâche à résoudre

### Tâche de base

Vous devez écrire des fonctions pour la génération de paroles de chansons traditionnelles pour enfants. Ces paroles ont la particularité d'être souvent répétitives. Prenez par exemple deux couplets de _Mort et convoi de l'invincible Malbrough_:

```
Malbrough s'en va-t-en guerre
Mironton, mironton, mirontaine
Malbrough s'en va-t-en guerre
Ne sait quand reviendra
Ne sait quand reviendra
Ne sait quand reviendra

Il reviendra z'à Pâques
Mironton, mironton, mirontaine
Il reviendra z'à Pâques
Ou à la Trinité
Ou à la Trinité
Ou à la Trinité
```

Vous voyez qu'un couplet a toujours la même structure: un vers spécifique au couplet $i$, appelons-le $v_{i1}$ (ici, pour le premier couplet, `"Malbrough s'en va-t-en guerre"`), puis le vers fixe `"Mironton, mironton, mirontaine"`, puis de nouveau $v_{i1}$, suivi de 3 répétitions d'un autre vers spécifique au couplet $i$, appelons-le $v_{i2}$ (ici, pour le premier couplet, `"Ne sait quand reviendra"`). Une fois cette structure connue, pour générer ces deux couplets (et les suivants), il suffit de connaître les $v_{i1}$ et $v_{i2}$.

**Partie (a)**

Écrivez une fonction `def malbrough(spec: List[Tuple[str, str]]) -> List[str]`. Sur la base d'une spécification de la chanson sous forme de liste de tuples d'arité 2 représentant une liste de couplets avec $v_{i1}$ et $v_{i2}$ pour chacun, générez et retournez une liste contenant chaque vers sous forme de string, avec un string vide qui indique la fin d'un couplet. Par exemple, ceci:

```python
malbrough([
    ("Malbrough s'en va-t-en guerre", "Ne sait quand reviendra"),
    ("Il reviendra z'à Pâques", "Ou à la Trinité"),
    ("La Trinité se passe", "Malbrough ne revient pas"),
    ("Madame à sa tour monte", "Si haut qu'elle peut monter")
])
```

… devra afficher ceci:

```python
[ "Malbrough s'en va-t-en guerre", 'Mironton, mironton, mirontaine',
  "Malbrough s'en va-t-en guerre", 'Ne sait quand reviendra',
  'Ne sait quand reviendra', 'Ne sait quand reviendra',
  '',
  "Il reviendra z'à Pâques", 'Mironton, mironton, mirontaine',
  "Il reviendra z'à Pâques", 'Ou à la Trinité',
  'Ou à la Trinité', 'Ou à la Trinité',
  '',
  'La Trinité se passe', 'Mironton, mironton, mirontaine',
  'La Trinité se passe', 'Malbrough ne revient pas',
  'Malbrough ne revient pas', 'Malbrough ne revient pas',
  '',
  'Madame à sa tour monte', 'Mironton, mironton, mirontaine',
  'Madame à sa tour monte', "Si haut qu'elle peut monter",
  "Si haut qu'elle peut monter", "Si haut qu'elle peut monter",
  '' ]
```

Vous constaterez que les deux premiers couplets sont, si affichés ligne par ligne, identiques à l'exemple texte ici en haut.

**Partie (b)**

Écrivez une fonction `def print_lyrics(lines: List[str])` qui prend en entrée une liste de vers, et les affiche un à un, ceci en faisant en sorte que deux vers identiques répétés l'un après l'autre ne s'affichent pas sur deux lignes, mais sur une seule ligne avec le suffixe `" (bis)"` pour signaler la répétition. De même, trois mêmes vers consécutifs seront affichées sur une seule ligne avec le suffixe `" (ter)"`, puis `" (quater)"` pour quatre, et `" (x fois)"` si le nombre de répétition $x \geq 5$, en remplaçant `x` par le bon nombre.

Ainsi, si on lui passe la liste générée par l'appel à la fonction `malbrough` ci-dessus, la fonction `print_lyrics` devra afficher ceci (montré pour les deux premiers couplets uniquement):

```
Malbrough s'en va-t-en guerre
Mironton, mironton, mirontaine
Malbrough s'en va-t-en guerre
Ne sait quand reviendra (ter)

Il reviendra z'à Pâques
Mironton, mironton, mirontaine
Il reviendra z'à Pâques
Ou à la Trinité (ter)
```

**Partie (c)**

Écrivez une fonction `lorraine` qui permet, de manière similaire, de générer les paroles d'_En passant par la Lorraine_ à partir d'une spécification compacte, que vous devez choisir (pas forcément une `List[Tuple[str, str]]`, donc). Appelez ensuite votre fonction, qui doit générer une liste de strings comme `malbrough`, puis passez le résultat à `print_lyrics`. Cela devrait afficher ceci, en prenant les trois premiers couplets:

```
En passant par la Lorraine avec mes sabots (bis)
Rencontrai trois capitaines
Avec mes sabots, dondaine
Oh oh oh, avec mes sabots

Rencontrai trois capitaines avec mes sabots (bis)
Ils m'ont appelée vilaine
Avec mes sabots, dondaine
Oh oh oh, avec mes sabots

Ils m'ont appelée vilaine avec mes sabots (bis)
Je ne suis pas si vilaine
Avec mes sabots, dondaine
Oh oh oh, avec mes sabots
```

Notez comme chaque couplet, à part le premier, reprend une phrase du dernier couplet. Attention, les _bis_ doivent être générés (comme les _ter_ avant) par `print_lyrics`. La fonction `lorraine` doit générer tous les vers individuellement.

**Partie (d)**

Livrez-vous au même exercice et écrivez une fonction `pied_mariton` pour générer les paroles d'_Un pied mariton_:

```
La Marie-Madeleine, elle a un pied mariton
Un pied mariton
Un pied mariton Madeleine, un pied mariton Madelon (bis)

La Marie-Madeleine, elle a une jambe de boué
Une jambe de boué
Un pied mariton
Un pied mariton Madeleine, un pied mariton Madelon (bis)

La Marie-Madeleine, elle a un genou cagneux
Un genou cagneux
Une jambe de boué
Un pied mariton
Un pied mariton Madeleine, un pied mariton Madelon (bis)

La Marie-Madeleine, elle a une cuisse de v'lours
Une cuisse de v'lours
Un genou cagneux
Une jambe de boué
Un pied mariton
Un pied mariton Madeleine, un pied mariton Madelon (bis)
```

Notez comme chaque couplet devient plus long que le précédent, en y incluant l'ensemble des éléments introduits aux couplets précédents, et que le dernier vers de chaque couplet répète deux fois l'élément introduit au premier couplet (ici, le «pied mariton»).

### Extension

**Partie (e)**

Vous avez écrit trois fonctions différentes pour ces trois chansons. L'implémentation de chacune de ces fonctions était différente, mais suivait une structure similaire de génération de vers. Écrivez une fonction plus générale `gen_lyrics`, dont l'implémentation est plus flexible, et qui est capable de générer les paroles de ces trois chansons à partir d'arguments similaires à ceux qui étaient passés à `malbrough`, `lorraine` et `pied_mariton`, respectivement, avec l'ajout de paramètres supplémentaires si nécessaire. Ces paramètres peuvent être des strings, des ints, des listes, des dictionnaires, etc. selon vos besoin, mais pas des fonctions.

Notez que `gen_lyrics` se doit d'avoir une forme générale. Cette fonction ne doit pas, par exemple, regarder si le premier élément fourni dans les paramètre est `"Malbrough s'en va-t-en guerre"` pour ensuite appliquer la structure de `malbrough`. Cette fonction devrait notamment être capable de générer les paroles de _L'Empereur, sa femme et le petit prince_, ou des _Chevaliers de la table ronde_, dont voici quelques couplets:

```
Chevaliers de la table ronde
Goûtons voir si le vin est bon
Goûtons voir, oui, oui, oui
Goûtons voir, non, non, non
Goûtons voir si le vin est bon

S'il est bon, s'il est agréable
J'en boirai jusqu'à mon plaisir
J'en boirai, oui, oui, oui
J'en boirai, non, non, non
J'en boirai jusqu'à mon plaisir

J'en boirai cinq à six bouteilles
Une femme sur mes genoux
Une femme, oui, oui, oui
Une femme, non, non, non
Une femme sur mes genoux
```

Notez qu'il y a beaucoup d'approches possibles pour résoudre ce problème. Lors de la présentation orale, il vous sera demandé de décrire votre approche, ses avantages, ses limites. Il vous est demandé de travailler en groupe de deux, mais de ne pas partager d'information entre les groupes sur la stratégie que vous allez employer ni de partager du code — entre étudiants de cette année-ci ou des années précédentes. Du code copié-collé d'un autre projet serait tout de suite détecté par la comparaison automatique des projets.

Si vous avez de la difficulté à implémenter une version très générale de `gen_lyrics`, décidez de comment vous désirez de vous simplifier la tâche (par exemple: «les couplets générables par cette fonction suivent forcément tel ou tel format») et résolvez cette tâche simplifiée. Vous pourrez quand même obtenir une partie de points si vos idées sont bonnes.

**Partie (f)**

Sur la base de ce que vous avez déjà fait pour `print_lyrics`, déclarez et implémentez une fonction `print_lyrics_with_chorus` qui soit en mesure, en plus de détecter les répétitions comme plus haut, de détecter un éventuel refrain. On part du principe que le refrain est défini comme le _premier_ couplet qui est entièrement répété plus tard dans la chanson. À la première apparition du refrain, insérez une ligne `"Refrain:"` avant son premier vers. Pour toutes les apparitions suivantes, remplacez tout le refrain par simplement la ligne `"(Refrain)"`.

Ainsi, cet appel:

```python
print_lyrics_with_chorus([
    "Au jardins de mon père, les lilas sont fleuris",
    "Au jardins de mon père, les lilas sont fleuris",
    "Tous les oiseaux du monde viennent y fair' leurs nids",
    "",
    "Auprès de ma blonde",
    "Qu'il fait bon, fait bon, fait bon",
    "Auprès de ma blonde",
    "Qu'il fait bon dormir",
    "",
    "La caille, la tourterelle et la jolie perdrix",
    "La caille, la tourterelle et la jolie perdrix",
    "Et la blanche colombe, qui chante jour et nuit",
    "",
    "Auprès de ma blonde",
    "Qu'il fait bon, fait bon, fait bon",
    "Auprès de ma blonde",
    "Qu'il fait bon dormir",
    ""
])
```

… devra afficher ceci:

```
Au jardins de mon père, les lilas sont fleuris (bis)
Tous les oiseaux du monde viennent y fair' leurs nids

Refrain:
Auprès de ma blonde
Qu'il fait bon, fait bon, fait bon
Auprès de ma blonde
Qu'il fait bon dormir

La caille, la tourterelle et la jolie perdrix (bis)
Et la blanche colombe, qui chante jour et nuit

(Refrain)
```

## Critères d'évaluation

L'évaluation du travail rendu se fera sur la base de plusieurs critères, dont:

  * Compréhension générale du problème à résoudre
  * Justesse du code présenté pour résoudre la tâche de base
  * Style du code présenté pour résoudre la tâche de base
  * Prise de parole égale des deux membres du groupe
  * Pertinence de l'approche choisie pour l'extension
  * Implémentation de la proposition d'extension
  * Capacité à suggérer des solutions alternatives et à évaluer leurs avantages et inconvénients
  * Aisance générale à utiliser les concepts de programmation et les structures de données de base de Python
